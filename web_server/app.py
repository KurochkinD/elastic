from flask import Flask, request
import logging

app = Flask(__name__)
logging.basicConfig(filename='logs/app.log', level=logging.DEBUG)


@app.route('/')
def hello_world():
    return 'Hello, World!'


@app.route('/log')
def log():
    log_message = request.args.get('message')
    logging.info(log_message)
    return 'Logged message: ' + log_message


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')